#include <stdio.h>
#include <stdlib.h>


#ifndef MEMORIA_H_INCLUDED
#define MEMORIA_H_INCLUDED

typedef struct registro
{
    int numero_registro;
    int valor;
    int envenenamiento;
    int in_use;
    int cores_in_use [4];         // Define cuales cores estan utilizando la posicion de la memoria
    int msi_state;               // Define en que estado se encuentra el valor del registor de cache
    unsigned long last_write;
    pthread_mutex_t lock;           // Cerrojo para la proteccion del registro (serializar)
    struct thread *next;
    struct thread *prev;
} *Registro;

typedef struct memoria
{
    Registro head;
    Registro tail;
    int tamanio;
} *Memoria;


Registro buscar_registro(Memoria list, int numero_registro);
void agregar_registro(Registro node, Memoria list);
void initMem();
// void eliminar_registro(Memoria list, long numero_registro);
// Thread pop_primer_thread(Memoria list);


#endif // MEMORIA_H_INCLUDED
