#include "pthread.h"
#include "thread_data.h"
#include "memoria.h"
#include "core.h"
#include <stdint.h>
#include <inttypes.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#ifndef GLOBALES_H_INCLUDED
#define GLOBALES_H_INCLUDED

Memoria mem;

Core core_1;
Core core_2;
Core core_3;
Core core_4;

FILE* outputCore1;
FILE* outputCore2;
FILE* outputCore3;
FILE* outputCore4;

pthread_t memory_output;

int randomNum(int lower, int upper);
uint64_t GetTimeStamp();

#endif // GLOBALES_H_INCLUDED
