#include <pthread.h>
#include "memoria.h"


#ifndef CORE_H_INCLUDED
#define CORE_H_INCLUDED

// Struct de cada procesador
typedef struct core
{
    int core_id;             // ID para mantener referencia a los hilos
    Memoria cache;
    int operacion;
    pthread_t control;
    pthread_t coherence_control;
} *Core;

void initCPUs();
void initCore_0();
void initCore_1();
void initCore_2();
void initCore_3();

#endif // CORE_H_INCLUDED
