#include <pthread.h>

#ifndef _THREAD_DATA_H
#define _THREAD_DATA_H




typedef struct thread
{
    long thread_identificador;
    struct thread *next;
    struct thread *prev;
} *Thread;

typedef struct thread_list
{
    Thread head;
    Thread tail;
    int tamanio;
} *ThreadList;


void agregar_thread(Thread node, ThreadList list);
Thread buscar_nodo_thread(ThreadList list, long thread_identificador);
void eliminar_nodo_thread(ThreadList list, long thread_identificador);
Thread pop_primer_thread(ThreadList list);


#endif
