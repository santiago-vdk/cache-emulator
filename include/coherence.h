#include <stdio.h>
#include <stdlib.h>
#include "core.h"
#include "memoria.h"
#include "globales.h"

#ifndef COHERENCE_H_INCLUDED
#define COHERENCE_H_INCLUDED

void *algoritmo_coherence(void *core);

#endif // COHERENCE_H_INCLUDED
