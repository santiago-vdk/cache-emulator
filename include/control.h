#include <stdio.h>
#include <stdlib.h>
#include "core.h"
#include "memoria.h"
#include "globales.h"

#ifndef CONTROL_H_INCLUDED
#define CONTROL_H_INCLUDED

void *algoritmo_control(void *core);

#endif // CONTROL_H_INCLUDED
