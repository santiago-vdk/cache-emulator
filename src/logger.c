#include "logger.h"

void initLogger(){
    
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
       printf("Current working dir: %s\n", cwd);
    } else {
       perror("getcwd() error");
       exit(1);
    }

    initLoggerCore1();
    initLoggerCore2();
    initLoggerCore3();
    initLoggerCore4();
    
}

void initLoggerCore1(){

    char buffer [1000];         // Buffer para el almacenado del comando
    snprintf(buffer, 1000, "gnome-terminal -x %s/scripts/core1_output_script.sh", cwd); // Concatenacion de strings
    puts (buffer);              // Actualizacion del valor del buffer

    fclose(fopen("output/core1_output", "w"));    // Clear del archivo
    system(buffer);     // Invocar logger del core 
    
}

void initLoggerCore2(){

    char buffer [1000];         // Buffer para el almacenado del comando
    snprintf(buffer, 1000, "gnome-terminal -x %s/scripts/core2_output_script.sh", cwd); // Concatenacion de strings
    puts (buffer);              // Actualizacion del valor del buffer

    fclose(fopen("output/core2_output", "w"));    // Clear del archivo
    system(buffer);     // Invocar logger del core 
    
}

void initLoggerCore3(){

    char buffer [1000];         // Buffer para el almacenado del comando
    snprintf(buffer, 1000, "gnome-terminal -x %s/scripts/core3_output_script.sh", cwd); // Concatenacion de strings
    puts (buffer);              // Actualizacion del valor del buffer

    fclose(fopen("output/core3_output", "w"));    // Clear del archivo
    system(buffer);     // Invocar logger del core 
    
}

void initLoggerCore4(){

    char buffer [1000];         // Buffer para el almacenado del comando
    snprintf(buffer, 1000, "gnome-terminal -x %s/scripts/core4_output_script.sh", cwd); // Concatenacion de strings
    puts (buffer);              // Actualizacion del valor del buffer

    fclose(fopen("output/core4_output", "w"));    // Clear del archivo
    system(buffer);     // Invocar logger del core 
    
}

