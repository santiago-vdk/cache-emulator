#include "memoria.h"
#include "globales.h"

void initMem()
{

    // Inicializacion de memoria
    mem = (Memoria) calloc(1, sizeof(struct memoria));
    Registro reg0 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg1 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg2 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg3 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg4 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg5 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg6 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg7 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg8 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg9 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg10 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg11 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg12 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg13 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg14 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg15 = (Registro) calloc(1, sizeof(struct registro));

    reg0->numero_registro = 0;
    reg1->numero_registro = 1;
    reg2->numero_registro = 2;
    reg3->numero_registro = 3;
    reg4->numero_registro = 4;
    reg5->numero_registro = 5;
    reg6->numero_registro = 6;
    reg7->numero_registro = 7;
    reg8->numero_registro = 8;
    reg9->numero_registro = 9;
    reg10->numero_registro = 10;
    reg11->numero_registro = 11;
    reg12->numero_registro = 12;
    reg13->numero_registro = 13;
    reg14->numero_registro = 14;
    reg15->numero_registro = 15;

    reg0->valor = 0;
    reg1->valor = 0;
    reg2->valor = 0;
    reg3->valor = 0;
    reg4->valor = 0;
    reg5->valor = 0;
    reg6->valor = 0;
    reg7->valor = 0;
    reg8->valor = 0;
    reg9->valor = 0;
    reg10->valor = 0;
    reg11->valor = 0;
    reg12->valor = 0;
    reg13->valor = 0;
    reg14->valor = 0;
    reg15->valor = 0;

    pthread_mutex_init(&reg0->lock, NULL);
    pthread_mutex_init(&reg1->lock, NULL);
    pthread_mutex_init(&reg2->lock, NULL);
    pthread_mutex_init(&reg3->lock, NULL);
    pthread_mutex_init(&reg4->lock, NULL);
    pthread_mutex_init(&reg5->lock, NULL);
    pthread_mutex_init(&reg6->lock, NULL);
    pthread_mutex_init(&reg7->lock, NULL);
    pthread_mutex_init(&reg8->lock, NULL);
    pthread_mutex_init(&reg9->lock, NULL);
    pthread_mutex_init(&reg10->lock, NULL);
    pthread_mutex_init(&reg11->lock, NULL);
    pthread_mutex_init(&reg12->lock, NULL);
    pthread_mutex_init(&reg13->lock, NULL);
    pthread_mutex_init(&reg14->lock, NULL);
    pthread_mutex_init(&reg15->lock, NULL);

    agregar_registro(reg0,mem);
    agregar_registro(reg1,mem);
    agregar_registro(reg2,mem);
    agregar_registro(reg3,mem);
    agregar_registro(reg4,mem);
    agregar_registro(reg5,mem);
    agregar_registro(reg6,mem);
    agregar_registro(reg7,mem);
    agregar_registro(reg8,mem);
    agregar_registro(reg9,mem);
    agregar_registro(reg10,mem);
    agregar_registro(reg11,mem);
    agregar_registro(reg12,mem);
    agregar_registro(reg13,mem);
    agregar_registro(reg14,mem);
    agregar_registro(reg15,mem);

}

/*
* Metodo para agregar hilos a una lista de hilos
*/
void agregar_registro(Registro node, Memoria list)
{

    node->prev = NULL;
    node->next = NULL;
    if (list->head == NULL)
    {
        list->head = node;
        list->tail = node;
        list->tamanio += 1;
    }
    else
    {
        list->tail->next = node;
        node->prev = list->tail;
        list->tail = node;
        list->tamanio += 1;
    }

}

/*
* Metodo para buscar un hilo en una lista de hilos
*/
Registro buscar_registro(Memoria list, int numero_registro)
{
    if (list->head->numero_registro == numero_registro)
    {
        return list-> head;
    }
    else
    {
        Registro tmp = list->head;
        while (tmp->numero_registro != numero_registro)
        {

            tmp = tmp->next;
            if (!tmp)
            {
                return NULL;
            }
        }
        return tmp;
    }
}
