#include "coherence.h"
#include <time.h>
/**
*   Algoritmo encargado de la coherencia de datos entre los cores
**/
void *algoritmo_coherence(void *core)
{
    Core this_core = (Core) core;
    Memoria cache;
    int i;              // Contador para recorrer el cache
    int cores[4];
    while(1)
    {
        cache = this_core -> cache;
        i = 0;
        while(i < this_core -> cache -> tamanio){
            if(buscar_registro(cache, i)->in_use == 1){        
                // Se evaluan los registros en uso del cache
                if(buscar_registro(cache, i)->msi_state == 1 && buscar_registro(cache, i)->valor != buscar_registro(mem, i)->valor) {          
                    // En caso de que un registro ya no sea igual al valor en memoria se invalida
                    buscar_registro(cache, i)->msi_state = 2;   // Invalid state

                    if(this_core->core_id == 1){                        

                        outputCore1 = fopen("output/core1_output", "a");
                        fprintf(outputCore1,ANSI_COLOR_BLUE "Registro %d invalidado\n" ANSI_COLOR_RESET, i);
                        fclose(outputCore1);

                    } else if(this_core->core_id == 2){        

                        outputCore2 = fopen("output/core2_output", "a");
                        fprintf(outputCore2, ANSI_COLOR_GREEN "Registro %d invalidado\n" ANSI_COLOR_RESET, i);
                        fclose(outputCore2);
                    
                    } else if(this_core->core_id == 3){                

                        outputCore3 = fopen("output/core3_output", "a");
                        fprintf(outputCore3, ANSI_COLOR_YELLOW "Registro %d invalidado\n" ANSI_COLOR_RESET, i);
                        fclose(outputCore3);
                    
                    } else {                            

                        outputCore4 = fopen("output/core4_output", "a");
                        fprintf(outputCore4, ANSI_COLOR_MAGENTA "Registro %d invalidado\n" ANSI_COLOR_RESET, i);
                        fclose(outputCore4);
                    
                    }
                } else if(buscar_registro(cache, i)->msi_state == 0 && buscar_registro(cache, i)->valor != buscar_registro(mem, i)->valor) {    
                    // Si lo comienza a usar otro Core
                    memcpy(cores, buscar_registro(mem,i)->cores_in_use, sizeof(cores));
                    
                    if( (cores[0] != 0 || cores[0] != this_core->core_id) ||
                        (cores[1] != 0 || cores[1] != this_core->core_id) ||
                        (cores[2] != 0 || cores[2] != this_core->core_id) ||
                        (cores[3] != 0 || cores[3] != this_core->core_id)){
                        // El Registro esta ahora siendo utilizado por otro core
                        buscar_registro(cache, i)->msi_state = 1;   // Shared state

                        if(this_core->core_id == 1){                        

                            outputCore1 = fopen("output/core1_output", "a");
                            fprintf(outputCore1,ANSI_COLOR_BLUE "Registro %d Shared\n" ANSI_COLOR_RESET, i);
                            fclose(outputCore1);

                        } else if(this_core->core_id == 2){        

                            outputCore2 = fopen("output/core2_output", "a");
                            fprintf(outputCore2, ANSI_COLOR_GREEN "Registro %d Shared\n" ANSI_COLOR_RESET, i);
                            fclose(outputCore2);
                        
                        } else if(this_core->core_id == 3){                

                            outputCore3 = fopen("output/core3_output", "a");
                            fprintf(outputCore3, ANSI_COLOR_YELLOW "Registro %d Shared\n" ANSI_COLOR_RESET, i);
                            fclose(outputCore3);
                        
                        } else {                            

                            outputCore4 = fopen("output/core4_output", "a");
                            fprintf(outputCore4, ANSI_COLOR_MAGENTA "Registro %d Shared\n" ANSI_COLOR_RESET, i);
                            fclose(outputCore4);
                        
                        }


                    }
                }
            }
            i ++;
        }
       usleep(10000);            
    }
    return NULL;
}