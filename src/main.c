#include <stdio.h>
#include <stdlib.h>
#include "core.h"
#include "memoria.h"
#include "globales.h"
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include "logger.h"
#include <termios.h>


int mygetch ( void ) 
{
  int ch;
  struct termios oldt, newt;

  tcgetattr ( STDIN_FILENO, &oldt );
  newt = oldt;
  newt.c_lflag &= ~( ICANON | ECHO );
  tcsetattr ( STDIN_FILENO, TCSANOW, &newt );
  ch = getchar();
  tcsetattr ( STDIN_FILENO, TCSANOW, &oldt );

  return ch;
}
void *memory_output_function(void *thread){
    
    while(1){
        // Para el estado del registro (compartido,...) cada registro de mem debe tener una lista donde cada core (coherence module) se agrega solo
        // y se saca solo por el envenenamiento
        printf("_____________________________________________________________Valor del Registro_____________________________________________________________\n");
        printf("-----|0|-----|1|-----|2|-----|3|-----|4|-----|5|-----|6|-----|7|-----|8|-----|9|-----|10|-----|11|-----|12|-----|13|-----|14|-----|15|-----\n");
        printf("-----|%d|-----|%d|-----|%d|-----|%d|-----|%d|-----|%d|-----|%d|-----|%d|-----|%d|------|%d|------|%d|------|%d|------|%d|------|%d|------|%d|------|%d|-----\n"
        , buscar_registro(mem,0)->valor, buscar_registro(mem,1)->valor, buscar_registro(mem,2)->valor, buscar_registro(mem,3)->valor, buscar_registro(mem,4)->valor
        , buscar_registro(mem,5)->valor, buscar_registro(mem,6)->valor, buscar_registro(mem,7)->valor, buscar_registro(mem,8)->valor, buscar_registro(mem,9)->valor
        , buscar_registro(mem,10)->valor, buscar_registro(mem,11)->valor, buscar_registro(mem,12)->valor, buscar_registro(mem,13)->valor, buscar_registro(mem,14)->valor
        , buscar_registro(mem,15)->valor);
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        printf("\n \n \n");
        sleep(1);

    }
    return NULL;
}



int main()
{

    time_t t;
    /*
    unsigned long t1 = GetTimeStamp();
    unsigned long t2 = GetTimeStamp();
    if(difftime(t2, t1) >= 0){
        printf("T1 Primero, T2 Segundo\n");
    } else {
        printf("T2 Primero, T1 Segundo\n");
    }*/

    
    printf(ANSI_COLOR_MAGENTA "Inicializando outputs\n" ANSI_COLOR_RESET);
    initLogger();

    printf("Presiones cualquier tecla para comenzar la ejecucion\n");
    mygetch();

    printf(ANSI_COLOR_MAGENTA "Inicializando memoria\n" ANSI_COLOR_RESET);
    initMem();
    pthread_create(&memory_output, NULL, memory_output_function, (void *) NULL);

    printf(ANSI_COLOR_MAGENTA "Inicializando semilla para random\n" ANSI_COLOR_RESET);
    srand((unsigned) time(&t));

    printf(ANSI_COLOR_MAGENTA "Inicializando cores\n" ANSI_COLOR_RESET);
    printf("\n");
    usleep(10000);

    initCPUs();

    // Iniciar espera de Threads de control de los Cores
    pthread_join(core_1->control, NULL);
    pthread_join(core_2->control, NULL);
    pthread_join(core_3->control, NULL);
    pthread_join(core_4->control, NULL);

    // Iniciar espera de Threads de coherence de los Cores
    pthread_join(core_1->coherence_control, NULL);
    pthread_join(core_2->coherence_control, NULL);
    pthread_join(core_3->coherence_control, NULL);
    pthread_join(core_4->coherence_control, NULL);

    pthread_join(memory_output, NULL);

    printf(ANSI_COLOR_RED "Liberando espacio asignado a cerrojos\n" ANSI_COLOR_RESET);
    int i = 0;
    while(i < mem->tamanio){
        pthread_mutex_destroy(buscar_registro(mem,i));
        i ++;
    }

    return 0;
}
