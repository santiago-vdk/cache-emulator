#include "control.h"

/**
*   Algoritmo encargado de la generacion de operaciones
**/
void *algoritmo_control(void *core)
{   
    Core this_core = (Core) core;
    Memoria cache = this_core->cache;

    while(1)
    {
        int peticion = randomNum(0,2);
        int registro_mem = -1;

        if(peticion == 0){                              // Peticion es de procesamiento
            int operando = randomNum(0,255);            // Operando con el que se operara el valor
            int registro_cache = randomNum(0,15);       // Registro en cache con el que se realizara la operacion
            int result = -1;
            char mensaje[12];

            Registro reg = buscar_registro(cache,registro_cache);   // Se obtiene el valor que actualmente esta en el cache
            if(reg->msi_state == 2 || reg->valor == 0){
                strcpy(mensaje, "Miss");
                reg = buscar_registro(mem,registro_cache); // Vamos a Memoria a buscar el dato correcto

                Registro reg_cache = buscar_registro(cache,registro_cache);
                pthread_mutex_lock(&reg_cache -> lock); 
                reg_cache->valor = reg->valor;      // Valor traido de memoria
                reg_cache->msi_state = 0;           // Marcamos la cache como Private
                pthread_mutex_unlock(&reg_cache -> lock);

            } else {
                strcpy(mensaje, "Hit");
            }

            if(this_core->core_id == 1){                         // AND

                result = operando&reg->valor;
                outputCore1 = fopen("output/core1_output", "a");
                fprintf(outputCore1, "Procesando %s de Registro %d, ", mensaje, registro_cache);
                fprintf(outputCore1,ANSI_COLOR_BLUE "operando %d AND %d con resultado %d\n" ANSI_COLOR_RESET,operando, reg->valor, result);
                fclose(outputCore1);

            } else if(this_core->core_id == 2){                  // OR

                result = operando|reg->valor;
                outputCore2 = fopen("output/core2_output", "a");
                fprintf(outputCore2, "Procesando %s de Registro %d, ", mensaje, registro_cache);
                fprintf(outputCore2, ANSI_COLOR_GREEN "operando %d AND %d con resultado %d\n" ANSI_COLOR_RESET,operando, reg->valor, result);
                fclose(outputCore2);
            
            } else if(this_core->core_id == 3){                  // XOR

                result = operando^reg->valor;
                outputCore3 = fopen("output/core3_output", "a");
                fprintf(outputCore3, "Procesando %s de Registro %d, ", mensaje, registro_cache);
                fprintf(outputCore3, ANSI_COLOR_YELLOW "operando %d AND %d con resultado %d\n" ANSI_COLOR_RESET,operando, reg->valor, result);
                fclose(outputCore3);
            
            } else {                                        // NOT + XOR

                result = ~reg->valor^operando;
                outputCore4 = fopen("output/core4_output", "a");
                fprintf(outputCore4, "Procesando %s de Registro %d, ", mensaje, registro_cache);
                fprintf(outputCore4, ANSI_COLOR_MAGENTA "operando %d AND %d con resultado %d\n" ANSI_COLOR_RESET,operando, reg->valor, result);
                fclose(outputCore4);
            
            }

        } else if(peticion == 1){                       // Peticion es de escritura
            registro_mem = randomNum(0,15);
            Registro reg = buscar_registro(mem,registro_mem);
            Registro reg_cache = buscar_registro(cache,registro_mem);
            

            // Write Through
            pthread_mutex_lock(&reg -> lock);                           // Obtencion del cerrojo
            reg->valor = this_core -> core_id;                          // Escritura en memoria
            reg->cores_in_use[(this_core -> core_id) - 1] = this_core->core_id;  // Asigno al arreglo de cores de la posicion de memoria en la posicion del core el valor del core 
            

            pthread_mutex_lock(&reg_cache -> lock);                     // Lock cerrojo en cache
            reg_cache->valor = this_core -> core_id;                    // Escritura en cache del core
            reg_cache->msi_state = 0;                                   // Private state: 1
            reg_cache -> in_use = 1;                                    // In Use state: 1, habilita el registro dentro del coherence
            reg_cache -> last_write = GetTimeStamp();         // Timestamp para prevenir error en coherence
            pthread_mutex_unlock(&reg_cache -> lock);                   // Unlock cerrojo en cache

            pthread_mutex_unlock(&reg -> lock);                         // Liberacion del cerrojo


            if(this_core->core_id == 1){

                outputCore1 = fopen("output/core1_output", "a");
                fprintf(outputCore1, ANSI_COLOR_CYAN "Escritura, ");
                fprintf(outputCore1,ANSI_COLOR_BLUE  "Registro %d en estado Private\n" ANSI_COLOR_RESET, registro_mem);
                fclose(outputCore1);

            } else if(this_core->core_id == 2){
                
                outputCore2 = fopen("output/core2_output", "a");
                fprintf(outputCore2, ANSI_COLOR_CYAN "Escritura, ");
                fprintf(outputCore2, ANSI_COLOR_GREEN "Registro %d en estado Private\n" ANSI_COLOR_RESET, registro_mem);
                fclose(outputCore2);

            } else if(this_core->core_id == 3){

                outputCore3 = fopen("output/core3_output", "a");
                fprintf(outputCore3, ANSI_COLOR_CYAN "Escritura, ");
                fprintf(outputCore3, ANSI_COLOR_YELLOW "Registro %d en estado Private\n" ANSI_COLOR_RESET, registro_mem);
                fclose(outputCore3);
                
            } else {

                outputCore4 = fopen("output/core4_output", "a");
                fprintf(outputCore4, ANSI_COLOR_CYAN "Escritura, ");
                fprintf(outputCore4, ANSI_COLOR_MAGENTA "Registro %d en estado Private\n" ANSI_COLOR_RESET, registro_mem);
                fclose(outputCore4);

            }


        } else {                                        // Peticion es de lectura
            // Si el estado MSI del registro en cache esta en estado S se puede leer, de no ser el caso se da un print de miss en el core y se va a memoria
            registro_mem = randomNum(0,15);
            int lectura = -1;
            char mensaje[6];

            if(buscar_registro(cache, registro_mem)->msi_state == 2 || buscar_registro(cache, registro_mem) ->valor == 0){
                // Lectura de memoria
                strcpy(mensaje, "Miss");
                lectura = buscar_registro(mem,registro_mem) -> valor;
         

            } else {
                
                   // Lectura de cache
                strcpy(mensaje, "Hit");
                lectura = buscar_registro(cache, registro_mem) -> valor;

            }

            buscar_registro(cache, registro_mem) -> in_use = 1;         // Marcamos el registro en uso para el manejador de coherencia

            if(this_core->core_id == 1){

                outputCore1 = fopen("output/core1_output", "a");
                fprintf(outputCore1, ANSI_COLOR_RED "Lectura %s, ", mensaje);
                fprintf(outputCore1, ANSI_COLOR_BLUE "Registro %d\n" ANSI_COLOR_RESET, registro_mem);
                fclose(outputCore1);

            } else if(this_core->core_id == 2){

                outputCore2 = fopen("output/core2_output", "a");
                fprintf(outputCore2, ANSI_COLOR_RED "Lectura %s, ", mensaje);
                fprintf(outputCore2, ANSI_COLOR_GREEN "Registro %d\n" ANSI_COLOR_RESET, registro_mem);
                fclose(outputCore2);

            } else if(this_core->core_id == 3){

                outputCore3 = fopen("output/core3_output", "a");
                fprintf(outputCore3, ANSI_COLOR_RED "Lectura %s, ", mensaje);
                fprintf(outputCore3, ANSI_COLOR_YELLOW "Registro %d\n" ANSI_COLOR_RESET, registro_mem);
                fclose(outputCore3);

            } else {

                outputCore4 = fopen("output/core4_output", "a");
                fprintf(outputCore4, ANSI_COLOR_RED "Lectura %s, ", mensaje);
                fprintf(outputCore4, ANSI_COLOR_MAGENTA "Registro %d\n" ANSI_COLOR_RESET, registro_mem);
                fclose(outputCore4);

            }

        }

        sleep(2);

    }

    return NULL;
}
