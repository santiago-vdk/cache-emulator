#include <stdlib.h>
#include <stdio.h>
#include "core.h"
#include "memoria.h"
#include "control.h"
#include "coherence.h"
#include "globales.h"

void initCPUs()
{

    core_1 = (Core) calloc(1, sizeof(struct core));
    core_1 -> core_id = 1;
    core_1 -> operacion = 1;
    initCore_1();

    core_2 = (Core) calloc(1, sizeof(struct core));
    core_2 -> core_id = 2;
    core_2 -> operacion = 2;
    initCore_2();

    core_3 = (Core) calloc(1, sizeof(struct core));
    core_3 -> core_id = 3;
    core_3 -> operacion = 3;
    initCore_3();

    core_4 = (Core) calloc(1, sizeof(struct core));
    core_4 -> core_id = 4;
    core_4 -> operacion = 4;
    initCore_4();
}

void initCore_1()
{
    // Inicializacion de memoria
    Memoria mem_cache_core_1 = (Memoria) calloc(1, sizeof(struct memoria));
    Registro reg0 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg1 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg2 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg3 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg4 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg5 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg6 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg7 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg8 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg9 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg10 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg11 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg12 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg13 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg14 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg15 = (Registro) calloc(1, sizeof(struct registro));

    reg0->numero_registro = 0;
    reg1->numero_registro = 1;
    reg2->numero_registro = 2;
    reg3->numero_registro = 3;
    reg4->numero_registro = 4;
    reg5->numero_registro = 5;
    reg6->numero_registro = 6;
    reg7->numero_registro = 7;
    reg8->numero_registro = 8;
    reg9->numero_registro = 9;
    reg10->numero_registro = 10;
    reg11->numero_registro = 11;
    reg12->numero_registro = 12;
    reg13->numero_registro = 13;
    reg14->numero_registro = 14;
    reg15->numero_registro = 15;

    reg0->valor = 0;
    reg1->valor = 0;
    reg2->valor = 0;
    reg3->valor = 0;
    reg4->valor = 0;
    reg5->valor = 0;
    reg6->valor = 0;
    reg7->valor = 0;
    reg8->valor = 0;
    reg9->valor = 0;
    reg10->valor = 0;
    reg11->valor = 0;
    reg12->valor = 0;
    reg13->valor = 0;
    reg14->valor = 0;
    reg15->valor = 0;

    reg0->envenenamiento = 0;
    reg1->envenenamiento = 0;
    reg2->envenenamiento = 0;
    reg3->envenenamiento = 0;
    reg4->envenenamiento = 0;
    reg5->envenenamiento = 0;
    reg6->envenenamiento = 0;
    reg7->envenenamiento = 0;
    reg8->envenenamiento = 0;
    reg9->envenenamiento = 0;
    reg10->envenenamiento = 0;
    reg11->envenenamiento = 0;
    reg12->envenenamiento = 0;
    reg13->envenenamiento = 0;
    reg14->envenenamiento = 0;
    reg15->envenenamiento = 0;

    reg0->in_use = 0;
    reg1->in_use = 0;
    reg2->in_use = 0;
    reg3->in_use = 0;
    reg4->in_use = 0;
    reg5->in_use = 0;
    reg6->in_use = 0;
    reg7->in_use = 0;
    reg8->in_use = 0;
    reg9->in_use = 0;
    reg10->in_use = 0;
    reg11->in_use = 0;
    reg12->in_use = 0;
    reg13->in_use = 0;
    reg14->in_use = 0;
    reg15->in_use = 0;

    pthread_mutex_init(&reg0->lock, NULL);
    pthread_mutex_init(&reg1->lock, NULL);
    pthread_mutex_init(&reg2->lock, NULL);
    pthread_mutex_init(&reg3->lock, NULL);
    pthread_mutex_init(&reg4->lock, NULL);
    pthread_mutex_init(&reg5->lock, NULL);
    pthread_mutex_init(&reg6->lock, NULL);
    pthread_mutex_init(&reg7->lock, NULL);
    pthread_mutex_init(&reg8->lock, NULL);
    pthread_mutex_init(&reg9->lock, NULL);
    pthread_mutex_init(&reg10->lock, NULL);
    pthread_mutex_init(&reg11->lock, NULL);
    pthread_mutex_init(&reg12->lock, NULL);
    pthread_mutex_init(&reg13->lock, NULL);
    pthread_mutex_init(&reg14->lock, NULL);
    pthread_mutex_init(&reg15->lock, NULL);

    agregar_registro(reg0,mem_cache_core_1);
    agregar_registro(reg1,mem_cache_core_1);
    agregar_registro(reg2,mem_cache_core_1);
    agregar_registro(reg3,mem_cache_core_1);
    agregar_registro(reg4,mem_cache_core_1);
    agregar_registro(reg5,mem_cache_core_1);
    agregar_registro(reg6,mem_cache_core_1);
    agregar_registro(reg7,mem_cache_core_1);
    agregar_registro(reg8,mem_cache_core_1);
    agregar_registro(reg9,mem_cache_core_1);
    agregar_registro(reg10,mem_cache_core_1);
    agregar_registro(reg11,mem_cache_core_1);
    agregar_registro(reg12,mem_cache_core_1);
    agregar_registro(reg13,mem_cache_core_1);
    agregar_registro(reg14,mem_cache_core_1);
    agregar_registro(reg15,mem_cache_core_1);

    core_1 ->cache = mem_cache_core_1;
    pthread_t control;
    pthread_create(&control, NULL, algoritmo_control, (void *) core_1);
    core_1 -> control = control;

    pthread_t coherence_control;
    pthread_create(&coherence_control, NULL, algoritmo_coherence, (void *) core_1);
    core_1 -> coherence_control = coherence_control;

}


void initCore_2()
{
    // Inicializacion de memoria
    Memoria mem_cache_core_2 = (Memoria) calloc(1, sizeof(struct memoria));
    Registro reg0 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg1 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg2 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg3 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg4 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg5 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg6 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg7 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg8 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg9 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg10 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg11 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg12 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg13 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg14 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg15 = (Registro) calloc(1, sizeof(struct registro));

    reg0->numero_registro = 0;
    reg1->numero_registro = 1;
    reg2->numero_registro = 2;
    reg3->numero_registro = 3;
    reg4->numero_registro = 4;
    reg5->numero_registro = 5;
    reg6->numero_registro = 6;
    reg7->numero_registro = 7;
    reg8->numero_registro = 8;
    reg9->numero_registro = 9;
    reg10->numero_registro = 10;
    reg11->numero_registro = 11;
    reg12->numero_registro = 12;
    reg13->numero_registro = 13;
    reg14->numero_registro = 14;
    reg15->numero_registro = 15;

    reg0->valor = 0;
    reg1->valor = 0;
    reg2->valor = 0;
    reg3->valor = 0;
    reg4->valor = 0;
    reg5->valor = 0;
    reg6->valor = 0;
    reg7->valor = 0;
    reg8->valor = 0;
    reg9->valor = 0;
    reg10->valor = 0;
    reg11->valor = 0;
    reg12->valor = 0;
    reg13->valor = 0;
    reg14->valor = 0;
    reg15->valor = 0;

    reg0->envenenamiento = 0;
    reg1->envenenamiento = 0;
    reg2->envenenamiento = 0;
    reg3->envenenamiento = 0;
    reg4->envenenamiento = 0;
    reg5->envenenamiento = 0;
    reg6->envenenamiento = 0;
    reg7->envenenamiento = 0;
    reg8->envenenamiento = 0;
    reg9->envenenamiento = 0;
    reg10->envenenamiento = 0;
    reg11->envenenamiento = 0;
    reg12->envenenamiento = 0;
    reg13->envenenamiento = 0;
    reg14->envenenamiento = 0;
    reg15->envenenamiento = 0;

    reg0->in_use = 0;
    reg1->in_use = 0;
    reg2->in_use = 0;
    reg3->in_use = 0;
    reg4->in_use = 0;
    reg5->in_use = 0;
    reg6->in_use = 0;
    reg7->in_use = 0;
    reg8->in_use = 0;
    reg9->in_use = 0;
    reg10->in_use = 0;
    reg11->in_use = 0;
    reg12->in_use = 0;
    reg13->in_use = 0;
    reg14->in_use = 0;
    reg15->in_use = 0;

    pthread_mutex_init(&reg0->lock, NULL);
    pthread_mutex_init(&reg1->lock, NULL);
    pthread_mutex_init(&reg2->lock, NULL);
    pthread_mutex_init(&reg3->lock, NULL);
    pthread_mutex_init(&reg4->lock, NULL);
    pthread_mutex_init(&reg5->lock, NULL);
    pthread_mutex_init(&reg6->lock, NULL);
    pthread_mutex_init(&reg7->lock, NULL);
    pthread_mutex_init(&reg8->lock, NULL);
    pthread_mutex_init(&reg9->lock, NULL);
    pthread_mutex_init(&reg10->lock, NULL);
    pthread_mutex_init(&reg11->lock, NULL);
    pthread_mutex_init(&reg12->lock, NULL);
    pthread_mutex_init(&reg13->lock, NULL);
    pthread_mutex_init(&reg14->lock, NULL);
    pthread_mutex_init(&reg15->lock, NULL);

    agregar_registro(reg0,mem_cache_core_2);
    agregar_registro(reg1,mem_cache_core_2);
    agregar_registro(reg2,mem_cache_core_2);
    agregar_registro(reg3,mem_cache_core_2);
    agregar_registro(reg4,mem_cache_core_2);
    agregar_registro(reg5,mem_cache_core_2);
    agregar_registro(reg6,mem_cache_core_2);
    agregar_registro(reg7,mem_cache_core_2);
    agregar_registro(reg8,mem_cache_core_2);
    agregar_registro(reg9,mem_cache_core_2);
    agregar_registro(reg10,mem_cache_core_2);
    agregar_registro(reg11,mem_cache_core_2);
    agregar_registro(reg12,mem_cache_core_2);
    agregar_registro(reg13,mem_cache_core_2);
    agregar_registro(reg14,mem_cache_core_2);
    agregar_registro(reg15,mem_cache_core_2);

    core_2 ->cache = mem_cache_core_2;
    pthread_t control;
    pthread_create(&control, NULL, algoritmo_control, (void *) core_2);
    core_2 -> control = control;

    pthread_t coherence_control;
    pthread_create(&coherence_control, NULL, algoritmo_coherence, (void *) core_2);
    core_2 -> coherence_control = coherence_control;

}


void initCore_3()
{
    // Inicializacion de memoria
    Memoria mem_cache_core_3 = (Memoria) calloc(1, sizeof(struct memoria));
    Registro reg0 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg1 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg2 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg3 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg4 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg5 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg6 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg7 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg8 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg9 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg10 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg11 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg12 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg13 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg14 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg15 = (Registro) calloc(1, sizeof(struct registro));

    reg0->numero_registro = 0;
    reg1->numero_registro = 1;
    reg2->numero_registro = 2;
    reg3->numero_registro = 3;
    reg4->numero_registro = 4;
    reg5->numero_registro = 5;
    reg6->numero_registro = 6;
    reg7->numero_registro = 7;
    reg8->numero_registro = 8;
    reg9->numero_registro = 9;
    reg10->numero_registro = 10;
    reg11->numero_registro = 11;
    reg12->numero_registro = 12;
    reg13->numero_registro = 13;
    reg14->numero_registro = 14;
    reg15->numero_registro = 15;

    reg0->valor = 0;
    reg1->valor = 0;
    reg2->valor = 0;
    reg3->valor = 0;
    reg4->valor = 0;
    reg5->valor = 0;
    reg6->valor = 0;
    reg7->valor = 0;
    reg8->valor = 0;
    reg9->valor = 0;
    reg10->valor = 0;
    reg11->valor = 0;
    reg12->valor = 0;
    reg13->valor = 0;
    reg14->valor = 0;
    reg15->valor = 0;

    reg0->envenenamiento = 0;
    reg1->envenenamiento = 0;
    reg2->envenenamiento = 0;
    reg3->envenenamiento = 0;
    reg4->envenenamiento = 0;
    reg5->envenenamiento = 0;
    reg6->envenenamiento = 0;
    reg7->envenenamiento = 0;
    reg8->envenenamiento = 0;
    reg9->envenenamiento = 0;
    reg10->envenenamiento = 0;
    reg11->envenenamiento = 0;
    reg12->envenenamiento = 0;
    reg13->envenenamiento = 0;
    reg14->envenenamiento = 0;
    reg15->envenenamiento = 0;

    reg0->in_use = 0;
    reg1->in_use = 0;
    reg2->in_use = 0;
    reg3->in_use = 0;
    reg4->in_use = 0;
    reg5->in_use = 0;
    reg6->in_use = 0;
    reg7->in_use = 0;
    reg8->in_use = 0;
    reg9->in_use = 0;
    reg10->in_use = 0;
    reg11->in_use = 0;
    reg12->in_use = 0;
    reg13->in_use = 0;
    reg14->in_use = 0;
    reg15->in_use = 0;

    pthread_mutex_init(&reg0->lock, NULL);
    pthread_mutex_init(&reg1->lock, NULL);
    pthread_mutex_init(&reg2->lock, NULL);
    pthread_mutex_init(&reg3->lock, NULL);
    pthread_mutex_init(&reg4->lock, NULL);
    pthread_mutex_init(&reg5->lock, NULL);
    pthread_mutex_init(&reg6->lock, NULL);
    pthread_mutex_init(&reg7->lock, NULL);
    pthread_mutex_init(&reg8->lock, NULL);
    pthread_mutex_init(&reg9->lock, NULL);
    pthread_mutex_init(&reg10->lock, NULL);
    pthread_mutex_init(&reg11->lock, NULL);
    pthread_mutex_init(&reg12->lock, NULL);
    pthread_mutex_init(&reg13->lock, NULL);
    pthread_mutex_init(&reg14->lock, NULL);
    pthread_mutex_init(&reg15->lock, NULL);

    agregar_registro(reg0,mem_cache_core_3);
    agregar_registro(reg1,mem_cache_core_3);
    agregar_registro(reg2,mem_cache_core_3);
    agregar_registro(reg3,mem_cache_core_3);
    agregar_registro(reg4,mem_cache_core_3);
    agregar_registro(reg5,mem_cache_core_3);
    agregar_registro(reg6,mem_cache_core_3);
    agregar_registro(reg7,mem_cache_core_3);
    agregar_registro(reg8,mem_cache_core_3);
    agregar_registro(reg9,mem_cache_core_3);
    agregar_registro(reg10,mem_cache_core_3);
    agregar_registro(reg11,mem_cache_core_3);
    agregar_registro(reg12,mem_cache_core_3);
    agregar_registro(reg13,mem_cache_core_3);
    agregar_registro(reg14,mem_cache_core_3);
    agregar_registro(reg15,mem_cache_core_3);

    core_3 ->cache = mem_cache_core_3;
    pthread_t control;
    pthread_create(&control, NULL, algoritmo_control, (void *) core_3);
    core_3 -> control = control;

    pthread_t coherence_control;
    pthread_create(&coherence_control, NULL, algoritmo_coherence, (void *) core_3);
    core_3 -> coherence_control = coherence_control;
}

void initCore_4()
{
    // Inicializacion de memoria
    Memoria mem_cache_core_4 = (Memoria) calloc(1, sizeof(struct memoria));
    Registro reg0 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg1 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg2 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg3 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg4 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg5 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg6 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg7 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg8 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg9 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg10 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg11 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg12 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg13 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg14 = (Registro) calloc(1, sizeof(struct registro));
    Registro reg15 = (Registro) calloc(1, sizeof(struct registro));

    reg0->numero_registro = 0;
    reg1->numero_registro = 1;
    reg2->numero_registro = 2;
    reg3->numero_registro = 3;
    reg4->numero_registro = 4;
    reg5->numero_registro = 5;
    reg6->numero_registro = 6;
    reg7->numero_registro = 7;
    reg8->numero_registro = 8;
    reg9->numero_registro = 9;
    reg10->numero_registro = 10;
    reg11->numero_registro = 11;
    reg12->numero_registro = 12;
    reg13->numero_registro = 13;
    reg14->numero_registro = 14;
    reg15->numero_registro = 15;

    reg0->valor = 0;
    reg1->valor = 0;
    reg2->valor = 0;
    reg3->valor = 0;
    reg4->valor = 0;
    reg5->valor = 0;
    reg6->valor = 0;
    reg7->valor = 0;
    reg8->valor = 0;
    reg9->valor = 0;
    reg10->valor = 0;
    reg11->valor = 0;
    reg12->valor = 0;
    reg13->valor = 0;
    reg14->valor = 0;
    reg15->valor = 0;

    reg0->envenenamiento = 0;
    reg1->envenenamiento = 0;
    reg2->envenenamiento = 0;
    reg3->envenenamiento = 0;
    reg4->envenenamiento = 0;
    reg5->envenenamiento = 0;
    reg6->envenenamiento = 0;
    reg7->envenenamiento = 0;
    reg8->envenenamiento = 0;
    reg9->envenenamiento = 0;
    reg10->envenenamiento = 0;
    reg11->envenenamiento = 0;
    reg12->envenenamiento = 0;
    reg13->envenenamiento = 0;
    reg14->envenenamiento = 0;
    reg15->envenenamiento = 0;

    reg0->in_use = 0;
    reg1->in_use = 0;
    reg2->in_use = 0;
    reg3->in_use = 0;
    reg4->in_use = 0;
    reg5->in_use = 0;
    reg6->in_use = 0;
    reg7->in_use = 0;
    reg8->in_use = 0;
    reg9->in_use = 0;
    reg10->in_use = 0;
    reg11->in_use = 0;
    reg12->in_use = 0;
    reg13->in_use = 0;
    reg14->in_use = 0;
    reg15->in_use = 0;

    pthread_mutex_init(&reg0->lock, NULL);
    pthread_mutex_init(&reg1->lock, NULL);
    pthread_mutex_init(&reg2->lock, NULL);
    pthread_mutex_init(&reg3->lock, NULL);
    pthread_mutex_init(&reg4->lock, NULL);
    pthread_mutex_init(&reg5->lock, NULL);
    pthread_mutex_init(&reg6->lock, NULL);
    pthread_mutex_init(&reg7->lock, NULL);
    pthread_mutex_init(&reg8->lock, NULL);
    pthread_mutex_init(&reg9->lock, NULL);
    pthread_mutex_init(&reg10->lock, NULL);
    pthread_mutex_init(&reg11->lock, NULL);
    pthread_mutex_init(&reg12->lock, NULL);
    pthread_mutex_init(&reg13->lock, NULL);
    pthread_mutex_init(&reg14->lock, NULL);
    pthread_mutex_init(&reg15->lock, NULL);

    agregar_registro(reg0,mem_cache_core_4);
    agregar_registro(reg1,mem_cache_core_4);
    agregar_registro(reg2,mem_cache_core_4);
    agregar_registro(reg3,mem_cache_core_4);
    agregar_registro(reg4,mem_cache_core_4);
    agregar_registro(reg5,mem_cache_core_4);
    agregar_registro(reg6,mem_cache_core_4);
    agregar_registro(reg7,mem_cache_core_4);
    agregar_registro(reg8,mem_cache_core_4);
    agregar_registro(reg9,mem_cache_core_4);
    agregar_registro(reg10,mem_cache_core_4);
    agregar_registro(reg11,mem_cache_core_4);
    agregar_registro(reg12,mem_cache_core_4);
    agregar_registro(reg13,mem_cache_core_4);
    agregar_registro(reg14,mem_cache_core_4);
    agregar_registro(reg15,mem_cache_core_4);

    core_4 -> cache = mem_cache_core_4;
    pthread_t control;
    pthread_create(&control, NULL, algoritmo_control, (void *) core_4);
    core_4 -> control = control;

    pthread_t coherence_control;
    pthread_create(&coherence_control, NULL, algoritmo_coherence, (void *) core_4);
    core_4 -> coherence_control = coherence_control;
}


