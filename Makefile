DIRS=build output bin
OUTPUT= ./output/core1_output ./output/core2_output ./output/core3_output ./output/core4_output

all:	\
	src/main.c \
	src/coherence.c include/coherence.h \
	src/control.c include/control.h \
	src/core.c include/core.h \
	src/globales.c include/globales.h \
	src/memoria.c include/memoria.h \
	src/thread_data.c include/thread_data.h \
	src/logger.c include/logger.h 

	gcc -o build/globales.o -c src/globales.c -I./include/ 
	gcc -o build/coherence.o -c src/coherence.c -I./include/ 
	gcc -o build/main.o -c src/main.c -I./include/ 
	gcc -o build/core.o -c src/core.c -I./include/
	gcc -o build/memoria.o -c src/memoria.c -I./include/ 
	gcc -o build/control.o -c src/control.c -I./include/ 
	gcc -o build/thread_data.o -c src/thread_data.c -I./include/ 
	gcc -o build/logger.o -c src/logger.c -I./include/ 

	gcc -o ./bin/cache build/main.o build/control.o \
		build/core.o \
		build/globales.o \
		build/memoria.o \
		build/coherence.o \
		build/thread_data.o \
		build/logger.o -pthread -lm

		
$(shell mkdir -p $(DIRS))
$(shell touch $(OUTPUT))
